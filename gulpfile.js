var gulp   = require('gulp');
var jshint = require('gulp-jshint');
var jasmine = require('gulp-jasmine-phantom');
var reporters = require('jasmine-reporters');
var stripCode = require('gulp-strip-code');

gulp.task('lint', function() {
	return gulp.src('./src/v2.0/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('unitTests', function () {
	return gulp.src('./src/spec/*.js')
		.pipe(jasmine());
});

gulp.task('removeTestComments', function(){
	gulp.src(['./src/v2.0/maths.js'])
		.pipe(stripCode({
			start_comment: "test-code",
			end_comment: " end-test-code"
		}))
		.pipe(gulp.dest('build'));
});

gulp.task('build', ['lint', 'unitTests', 'removeTestComments']);
