(function () {
    'use strict';
	
	var maths = maths || {};
	
	maths.addInteger = function (sequence) {
		var operation = function(a, b) { return a + b; };
		return maths.calculationInteger(sequence, operation);
	};
	
	maths.multiplyInteger = function (sequence) {
		var operation = function(a, b) { return a * b; };
		return maths.calculationInteger(sequence, operation);
	};

	maths.calculationInteger = function(sequence, operation) {
		if (typeof sequence === "undefined" || sequence.length === 0) {
			return 0;
		}
		var result = maths.getInteger(sequence[0]);
		for (var i = 1, length = sequence.length; i < length; i++) {
			result = operation(result, maths.getInteger(sequence[i]));
		}

		return maths.presentOutput(sequence, result);
	};

	maths.presentOutput = function(input, output){
		var outputObj = {};
		outputObj.input = input;
		outputObj.output = output;

		return outputObj;
	};
	
	maths.getInteger = function (x) {
		var result = parseInt(x, 10);

		// result should fallback to 0 if 
		// x cannot be parsed
		if(isNaN(result)) {
			result = 0;
		}

		return result;
	};

	var doMaths = function (numbers) {
		for(var i in numbers) {
			if (numbers.hasOwnProperty(i)) {
				var value = numbers[i];
				
				var sum1 = maths.addInteger(value);
				console.log('Addition result = ' + sum1.output);
				
				var sum2 = maths.multiplyInteger(value);
				console.log("Multiplication result = " + sum2.output);
			}
		}
	};
	
	var data = {
		first : [3.57, 2.43, '043'],
		second : [7.26, 1.43, '025'],
		third : ['076', 3.0, 6.42],
	};
	
	doMaths(data);


})();