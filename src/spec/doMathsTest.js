var mathsFile = require("../v2.0/maths.js");
var mathsObj = mathsFile.mathsObj;

describe("JS Test Pass Conditions", function() {

  it("addInteger will return the correct output when supplied with the first sequence", function() {
    var sampleSequence = [3.57, 2.43, '043'];
    var addSequenceObj = {
      'input': sampleSequence,
      'output': 48
    };

    expect(mathsObj.addInteger(sampleSequence)).toEqual(addSequenceObj);
  });

  it("multiplyInteger will return the correct output when supplied with the first sequence", function() {
    var sampleSequence = [3.57, 2.43, '043'];
    var addSequenceObj = {
      'input': sampleSequence,
      'output': 258
    };

    expect(mathsObj.multiplyInteger(sampleSequence)).toEqual(addSequenceObj);
  });


  it("addInteger will return the correct output when supplied with the second sequence", function() {
    var sampleSequence = [7.26, 1.43, '025'];
    var addSequenceObj = {
      'input': sampleSequence,
      'output': 33
    };

    expect(mathsObj.addInteger(sampleSequence)).toEqual(addSequenceObj);
  });

  it("multiplyInteger will return the correct output when supplied with the second sequence", function() {
    var sampleSequence = [7.26, 1.43, '025'];
    var addSequenceObj = {
      'input': sampleSequence,
      'output': 175
    };

    expect(mathsObj.multiplyInteger(sampleSequence)).toEqual(addSequenceObj);
  });



  it("addInteger will return the correct output when supplied with the third sequence", function() {
    var sampleSequence = ['076', 3.0, 6.42];
    var addSequenceObj = {
      'input': sampleSequence,
      'output': 85
    };

    expect(mathsObj.addInteger(sampleSequence)).toEqual(addSequenceObj);
  });

  it("multiplyInteger will return the correct output when supplied with the third sequence", function() {
    var sampleSequence = ['076', 3.0, 6.42];
    var addSequenceObj = {
      'input': sampleSequence,
      'output': 1368
    };

    expect(mathsObj.multiplyInteger(sampleSequence)).toEqual(addSequenceObj);
  });

});
