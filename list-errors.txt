
Errors From Lint
	Please see Initial-Lint-Screen Shot 2016-05-06 at 11.41.24.png
	I can talk through these issues in person if required. 

General Errors
	1) The function 'domaths' is not written as cammelcase.
	2) Within doMaths the if statement is missing the bracket scope - it is a multi line statment so curly brackets are required.
	3) Within maths.addInteger the result vairable is not declared with var which means it is added to the global scope.
	4) Within the multiplyInteger method there is a loop in which the previous dev was re decalring the result variable they had already defined outside the loop. I have removed the var within the loop so that now the result variable is accessed from the parent function scope. 

Improvements and Refactoring

	1) There was repeated logic in presenting the output logic in addInteger and multiplyInteger so I have created another method called presentOutput.

	2) addInteger and multiplyInteger have a lot of shared code. This could be refactored and a new 'calcualteInteger' method could be called by both along with the sequence and the operation as parameters. DONE

	3) Variables within functions are decalred first so that they are not hoisted out of context.


Assumptions I have made
	If a non emptey array is given as the sequence then the addInteger and multiplyInteger methods will return 0 and not an output object.

	If an array entry cannot be parsed than its value will be 0.

	I have not refactored or added unit tests for the doMaths function or its data. 

	The code has to remain in an IFFE and that is why I have added test comments and a module export to the file within the IFFE.

	I have not tested for negative numbers / other edge cases. 