var mathsFile = require("../v2.0/maths.js");
var mathsObj = mathsFile.mathsObj;

describe("Math Object multiplyInteger", function() {
  it("contains a method called multiplyInteger", function() {
    expect(typeof mathsObj.multiplyInteger == 'function').toBe(true);
  });

  it("multiplyInteger will return an object when supplied with an array of integers", function() {
    var sampleSequence = [1, 2, 3, 4, 5];
    var addSequenceObj = {
      'input': sampleSequence,
      'output': 120
    }

    expect(mathsObj.multiplyInteger(sampleSequence)).toEqual(addSequenceObj);
  });

  it("multiplyInteger will return an object when supplied with an array of decimals", function() {
    var sampleSequence = [1.1, 2.5, 3.9];
    var addSequenceObj = {
      'input': sampleSequence,
      'output': 6
    }

    expect(mathsObj.multiplyInteger(sampleSequence)).toEqual(addSequenceObj);
  });
    it("multiplyInteger will return an object when supplied with an array of string integers", function() {
    var sampleSequence = ['1.1', '02', '03'];
    var addSequenceObj = {
      'input': sampleSequence,
      'output': 6
    }

    expect(mathsObj.multiplyInteger(sampleSequence)).toEqual(addSequenceObj);
  });

  it("multiplyInteger will return 0 when supplied with no sequence", function() {
    var sampleSequence;
    var addSequenceObj = 0;

    expect(mathsObj.multiplyInteger(sampleSequence)).toEqual(addSequenceObj);
  });
});