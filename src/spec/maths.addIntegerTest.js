var mathsFile = require("../v2.0/maths.js");
var mathsObj = mathsFile.mathsObj;

describe("Math Object addInteger", function() {
  it("contains a method called addInteger", function() {
    expect(typeof mathsObj.addInteger == 'function').toBe(true);
  });

  it("addInteger will return an object when supplied with an array of integers", function() {
    var sampleSequence = [1, 2, 3, 4, 5];
    var addSequenceObj = {
      'input': sampleSequence,
      'output': 15
    }

    expect(mathsObj.addInteger(sampleSequence)).toEqual(addSequenceObj);
  });

  it("addInteger will return an object when supplied with an array of decimals", function() {
    var sampleSequence = [1.1, 2.5, 3.9];
    var addSequenceObj = {
      'input': sampleSequence,
      'output': 6
    }

    expect(mathsObj.addInteger(sampleSequence)).toEqual(addSequenceObj);
  });

  it("addInteger will return an object when supplied with an array of string integers", function() {
    var sampleSequence = ['1.1', '02', '03'];
    var addSequenceObj = {
      'input': sampleSequence,
      'output': 6
    }

    expect(mathsObj.addInteger(sampleSequence)).toEqual(addSequenceObj);
  });

  it("addInteger will return 0 when supplied with no sequence", function() {
    var sampleSequence;
    var addSequenceObj = 0;

    expect(mathsObj.addInteger(sampleSequence)).toEqual(addSequenceObj);
  });

});