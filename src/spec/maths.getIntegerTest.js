var mathsFile = require("../v2.0/maths.js");
var mathsObj = mathsFile.mathsObj;

describe("Math Object getInteger", function() {
  it("contains a method called getInteger", function() {
    expect(typeof mathsObj.getInteger == 'function').toBe(true);
  });

  it("getInteger will return an integer value when supplied with an integer", function() {
      var a = 12;
    expect(mathsObj.getInteger(a)).toBe(12);
  });

  it("getInteger will return an integer value when supplied with a decimal", function() {
      var b = 12.3;
    expect(mathsObj.getInteger(b)).toBe(12);
  });

  it("getInteger will return an integer value when supplied with a decimal (dec value >0.5)", function() {
      var b = 12.9;
    expect(mathsObj.getInteger(b)).toBe(12);
  });

   it("getInteger will return an integer value when supplied with a string integer", function() {
      var c = '012';
    expect(mathsObj.getInteger(c)).toBe(12);
  });

  it("getInteger will return an integer value of 0 when supplied with something that cannot be parsed into an Integer", function() {
      var c = 'abc';
      var d = null;
      var e = [];
      expect(mathsObj.getInteger(c)).toBe(0);
      expect(mathsObj.getInteger(d)).toBe(0);
      expect(mathsObj.getInteger(e)).toBe(0);
  });

});





